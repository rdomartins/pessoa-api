package br.pessoa.controller;

import br.pessoa.repository.Pessoa;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/")
public class CadastroPessoaController {

	@RequestMapping(value = "/listar", method = RequestMethod.GET)
	public String listar(Model model) {
		List<Pessoa> lista = new ArrayList<Pessoa>();
		lista.add(new Pessoa(new BigInteger("1"), 1l, "Daniel", "Analista"));
		lista.add(new Pessoa(new BigInteger("2"),  2l, "Daniela", "Programadora"));
		model.addAttribute("pessoas", lista);
		return "lista";
	}

	@RequestMapping(value ="/salvar", method = RequestMethod.POST)
	public String salvar(Pessoa pessoa) {
		System.out.println("Pessoa " + pessoa.getNome() + " cadastrada!");
		return "redirect:/listar";
	}
}