package br.pessoa.controller;

import br.pessoa.repository.Pessoa;
import br.pessoa.repository.PessoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/pessoa")
public class PessoaController {
	@Autowired
	private PessoaService service;

	@PostMapping(value = "/salvar", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<?> salvar(@RequestBody Pessoa pessoa) {
		service.salvar(pessoa);
		return ResponseEntity
				.ok("Pessoa [" + pessoa.getNome() + ", " + pessoa.getCargo() + "] armazenada com sucesso!");
	}

	@GetMapping(value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public List<Pessoa> listar() {
		return service.findAll();
	} 

}