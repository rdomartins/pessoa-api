package br.pessoa.repository;

import java.math.BigInteger;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Document(collection = "pessoas")
public class Pessoa {
   @Id
   private BigInteger _id;

   @Field("codigo")
   private Long codigo;

   @Indexed
   @Field("nome")
   private String nome;

   @Indexed
   @Field("cargo")
   private String cargo;
}
