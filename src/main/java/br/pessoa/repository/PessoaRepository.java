package br.pessoa.repository;

import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PessoaRepository extends MongoRepository<Pessoa, Long> {
   public Optional<Pessoa> findByCodigo(Long codigo);
}