package br.pessoa.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PessoaService {
   @Autowired
   private PessoaRepository repository;
 
   public void salvar(Pessoa pessoa) {
      repository.save(pessoa);
   }
 
   public List<Pessoa> findAll() {
      return repository.findAll();
   }
 
   public long count() {
      return repository.count();
   }
 
   public void delete(Pessoa pessoa) {
      repository.delete(pessoa);
   }
 
   public Optional<Pessoa> findByCodigo(Long codigo) {
      return repository.findByCodigo(codigo);
   }
}